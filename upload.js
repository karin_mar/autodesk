/**
 * Created by karin.
 * This program opens a default browser, reads from a txt file that is on my computer and stores the content of the file.
 * Then an http request is sent. The file is uploaded and the status of the result is printed.
 */

//open the browser
var open = require('open');
open('http://cgi-lib.berkeley.edu/ex/fup.html');

var fs = require('fs');
var fileContent;
//reading the file
fs.readFile('MeirBanaiBiography.txt', function(error, data) {
    fileContent = data;
    //console.log("contents of file: "+ fileContent);

    var http = require('http');
    //upfile is the name of "Choose File" button
    var postData = 'upfile=' + fileContent;

    var PostMsg = {
        hostname: 'cgi-lib.berkeley.edu',
        port: 80,
        path: '/ex/fup.cgi',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': postData.length
        }
    };

    var req = http.request(PostMsg, function(res) {
        console.log('The status is: ' + res.statusCode);
        res.on('data', function (chunk) {
            //console.log('BODY: ' + chunk);
            console.log("The file was uploaded successfully!");
        });

    });

    req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
    });


    //write data to request body
    req.write(postData);
    req.end();

});

